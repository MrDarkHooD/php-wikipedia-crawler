<?php
function format_input_readline($input_text, $additional = null) {
  $return = "\033[1m\033[31m" . $input_text . "\033[0m\033[22m";
  $return .= ($additional) ? " (\e[3m" . $additional . "\e[0m): " : ": ";

  return $return;
}