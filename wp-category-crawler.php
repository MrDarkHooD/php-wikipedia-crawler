<?php
include("settings.php");
include("inc/functions.php");

function parse_wp_json($url, $filename) {
  $data = file_get_contents($url);
  $json = json_decode($data);

  /*
   * This should only happen if there is
   * not a single result at beginning.
   */
  if(!count($json->query->categorymembers))
    die("No results.\n");

  if($filename)
    $fp = fopen($filename, "a");
  
  /* Print page names in category */
  foreach($json->query->categorymembers as $page) {
    echo $page->title . "\n";
    if($filename)
      fwrite($fp, $page->title."\n");
  }
  if($filename)
    fclose($fp);
  
  /* Check if there is another page to read */
  if(isset($json->continue)) {

    /*
     * If url containts next page value we replace
     * it to new one. Otherwise we add it to end.
     */
    $next_page = $json->continue->cmcontinue;
    if(preg_match("/\?cmcontinue=(.*)/", $url, $current_next)) {
      $url = preg_replace("/$current_next/", $next_page, $url);
    }else {
      $url .= "&cmcontinue=" . $next_page;
    }
    parse_wp_json($url);
  }
}

echo "Welcome to Wikipedia category crawler!\n";
$category = readline(format_input_readline("Catergory"));
$alpha_2_code = readline(format_input_readline("Language Alpha-2 code", "Default: $def_alpha_2"));
$filename = readline(format_input_readline("File to save", "Empty: no saving"));
/*
 * If alpha-2 code is
 * not set, use default
 */
if(!$alpha_2_code)
  $alpha_2_code = $def_alpha_2;

/*
 * If category name does not have ":"
 * char, add default category name
 */
if(strpos($category, ":") === false)
  $category = "category:" . $category;

/* Generate url */
$url = "https://" . $alpha_2_code . ".wikipedia.org/w/api.php";
$url .= "?action=query&list=categorymembers";
$url .= "&cmtitle=" . urlencode($category);
$url .= "&cmlimit=" . $results_per_query . "&format=json";

/* Start crawling */
parse_wp_json($url, $filename);